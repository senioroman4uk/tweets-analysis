import numpy as np
from pyspark.ml.feature import Word2VecModel
from pyspark.sql import SparkSession
from sklearn.ensemble import IsolationForest
import pickle

APP_NAME = 'TRAIN_ISOLATION_FOREST'

# Configure Spark
spark_session = SparkSession.builder \
    .master("local[*]") \
    .appName(APP_NAME) \
    .config('spark.jars.packages', 'org.mongodb.spark:mongo-spark-connector_2.11:2.2.0') \
    .config("spark.mongodb.input.uri", "mongodb://127.0.0.1/TweetsAnalysis.TestTweets") \
    .getOrCreate()

spark_session.sparkContext.setLogLevel('ERROR')

df = spark_session.read.format("com.mongodb.spark.sql.DefaultSource").load()
word2vecModel = Word2VecModel.load('word2vectest.model')

# giant memory usage
result = word2vecModel.transform(df).collect()
keyword_features_matrix = np.matrix(list(map(lambda x: x['features'], result)))

rng = np.random.RandomState(42)

model = IsolationForest(max_samples=100, random_state=rng)
model.fit(keyword_features_matrix)
pickle.dump(model, open('isolation_forest.sav', 'wb'))
