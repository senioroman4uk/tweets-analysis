import json

from pykafka import KafkaClient
from pymongo import MongoClient

mongo_config = json.load(open('mongo.json'))
mongo_client = MongoClient(mongo_config['connectionString'])
db = mongo_client.get_database(mongo_config['database'])
tweets_collection = db.get_collection(mongo_config['collectionName'])

kafka_config = json.load(open('kafka.json'))
kafka_client = KafkaClient(hosts=kafka_config['hosts'], zookeeper_hosts=kafka_config['zookeeperHosts'])


topic_name = kafka_config['topic']
topic = kafka_client.topics[topic_name.encode()]


consumer = topic.get_balanced_consumer(b'training_consumer')
for msg in consumer:
    parsed_message = json.loads(msg.value)
    if parsed_message is None:
        continue

    # print(parsed_message)
    tweets_collection.insert_one(parsed_message)
