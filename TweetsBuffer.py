import threading


class TweetsBuffer:
    tweetsBuffer = []

    def __init__(self):
        self.lock = threading.Lock()

    def insert(self, tweet):
        self.lock.acquire()
        self.tweetsBuffer.append(tweet)
        self.lock.release()

    def pop(self):
        self.lock.acquire()
        tweet = self.tweetsBuffer.pop() if len(self.tweetsBuffer) > 0 else None
        self.lock.release()
        return tweet
