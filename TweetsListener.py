import logging

import re
from tweepy import StreamListener

from TweetsBuffer import TweetsBuffer


class TweetsListener(StreamListener):
    def __init__(self, tweets_buffer: TweetsBuffer):
        super().__init__()
        self.tweets_buffer = tweets_buffer

    def on_status(self, status):
        tweet = self._parse_status(status)
        self.tweets_buffer.insert(tweet)
        return True

    def on_limit(self, track):
        logging.warning('limit {}'.format(track))
        return True

    def on_error(self, status_code):
        logging.warning('unsuccessful status code {}'.format(status_code))
        return True

    def on_timeout(self):
        logging.error('connection timeout')
        return False

    def on_disconnect(self, notice):
        logging.error(notice)
        return False

    def on_warning(self, notice):
        logging.warning(notice)
        return True

    @staticmethod
    def _get_text(status) -> str:
        if hasattr(status, 'extended_tweet'):
            return status.extended_tweet['full_text']
        else:
            return status.text

    @staticmethod
    def _parse_status(status):
        # parse user object
        user = {
            'user_id': status.user.id,
            'screen_name': status.user.screen_name,
            'name': status.user.name,
            'followers_count': status.user.followers_count,
            'friends_count': status.user.friends_count,
            'description': status.user.description
            if not status.user.description is None
            else "N/A",
            'image_url': status.user.profile_image_url,
            'location': status.user.location
            if not status.user.location is None
            else "N/A",
            'created_at': status.user.created_at.isoformat()
            # to avoid Object of type 'datetime' is not JSON serializable
        }

        if hasattr(status, 'retweeted_status'):
            text = TweetsListener._get_text(status.retweeted_status)
        else:
            text = TweetsListener._get_text(status)

        text = text.replace('&amp;', '&').replace('\n', ' ')
        # remove urls
        text = re.sub(r"http\S+", "", text)

        tweet = {
            'tweet_id': status.id,
            'tweet_text': text,
            'created_at': status.created_at.isoformat(),  # to avoid Object of type 'datetime' is not JSON serializable
            'geo_lat': status.coordinates['coordinates'][0]
            if not status.coordinates is None
            else 0,
            'geo_long': status.coordinates['coordinates'][1]
            if not status.coordinates is None
            else 0,
            'user_id': status.user.id,
            'tweet_url': "http://twitter.com/" + status.user.id_str + "/status/" + status.id_str,
            'retweet_count': status.retweet_count,
            'urls': status.entities['urls'],
            'hashtags': status.entities['hashtags'],
            'mentions': status.entities['user_mentions'],
            'user': user
        }

        return tweet

