import json
import pickle

from pyspark import SparkContext
from pyspark.ml.feature import Word2VecModel
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import BooleanType, ArrayType, DoubleType
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

APP_NAME = "Tweets_processing"


def predict(word2vec, random_forest):
    def for_rdd(rdd):
        if rdd.isEmpty():
            return

        # if isolation forest returns -1 it means we have outliners
        prediction_udf = udf(lambda features: random_forest.predict([features])[0].item() == -1, BooleanType())
        vector_to_list_udf = udf(lambda x: x.toArray().tolist(), ArrayType(DoubleType()))
        df = word2vec.transform(rdd.toDF()).withColumn('prediction', prediction_udf('features'))\
            .withColumn('features_array', vector_to_list_udf('features'))\
            .drop('features') \
            .withColumnRenamed("features_array", "features")
        df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").save()

    return for_rdd


def main(sc: SparkContext, kafka_config):
    word2vecModel = Word2VecModel.load('word2vectest.model')
    isolation_forest_model = pickle.load(open('isolation_forest.sav', 'rb'))

    streaming_context = StreamingContext(sc, 10)
    tweets_stream = KafkaUtils.createDirectStream(streaming_context, [kafka_config['topic']],
                                                  {
                                                      "metadata.broker.list": kafka_config['hosts'],
                                                  },
                                                  valueDecoder=json.loads)
    tweets_stream \
        .map(lambda pair: pair[1]) \
        .foreachRDD(predict(word2vecModel, isolation_forest_model))

    streaming_context.start()
    streaming_context.awaitTermination()


if __name__ == "__main__":
    # Configure Spark
    spark_session = SparkSession.builder \
        .master("local[*]") \
        .appName(APP_NAME) \
        .config('spark.jars.packages', 'org.apache.spark:spark-streaming-kafka-0-8_2.11:2.2.0,org.mongodb.spark:mongo-spark-connector_2.11:2.2.0') \
        .config('spark.mongodb.output.uri', 'mongodb://127.0.0.1/TweetsAnalysis.PredictedTweets') \
        .getOrCreate()

    spark_session.sparkContext.setLogLevel('ERROR')
    kafka_config = json.load(open('kafka.json'))

    main(spark_session.sparkContext, kafka_config)
