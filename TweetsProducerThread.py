import queue
import threading
import time
import json
import logging

from rake_nltk import Rake
from pykafka import Producer

from TweetsBuffer import TweetsBuffer


class TweetsProducerThread(threading.Thread):
    def __init__(self, tweets_buffer: TweetsBuffer, producer: Producer, process_delivery=False):
        threading.Thread.__init__(self)
        self._tweets_buffer = tweets_buffer
        self._producer = producer
        self._count = 0
        self._process_delivery = process_delivery

    def run(self):
        print("Start processing tweets...")
        try:
            while True:
                tweet = self._tweets_buffer.pop()
                if tweet is None:
                    time.sleep(3)
                    continue

                rake = Rake()
                rake.extract_keywords_from_text(tweet['tweet_text'])
                tweet['keywords'] = rake.get_ranked_phrases()[:5]

                self._producer.produce(json.dumps(tweet).encode())
                if self._process_delivery:
                    self._count += 1
                    if self._count == 20:
                        self.process_delivery_report()
                        self._count = 0
        except KeyboardInterrupt:
            pass

        print("Keyboard interrupt. Stop processing tweets...")

    def process_delivery_report(self):
        try:
            while True:
                msg, exc = self._producer.get_delivery_report(block=False)
                if exc is not None:
                    logging.error('Failed to deliver msg {}: {}'.format(msg.offset, repr(exc)))
        except queue.Empty:
            pass
