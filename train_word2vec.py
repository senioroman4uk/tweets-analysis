import json

from pyspark.ml.feature import Word2Vec
from pyspark.sql import SparkSession
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

APP_NAME = "Tweets_processing"


def main(sc: SparkSession, kafka_config):
    df = sc.read.format("com.mongodb.spark.sql.DefaultSource").load()
    df.printSchema()
    word2Vec = Word2Vec(vectorSize=3, minCount=0, inputCol="keywords", outputCol="features")
    model = word2Vec.fit(df)
    model.save('word2vectest.model')
    # streaming_context = StreamingContext(sc.sparkContext, 10)
    # tweets_stream = KafkaUtils.createDirectStream(streaming_context, [kafka_config['topic']],
    #                                               {"metadata.broker.list": kafka_config['hosts']},
    #                                               valueDecoder=json.loads)
    # tweets = tweets_stream.map(lambda x: x[1])
    # tweets_dataframe = sc.createDataFrame([])
    # tweets.foreachRDD(lambda rdd: tweets_dataframe.union(sc.createDataFrame(rdd)))
    # word2Vec = Word2Vec(vectorSize=3, minCount=0, inputCol="tweet_text", outputCol="features")
    # model = word2Vec.fit(tweets_dataframe)
    # word2Vec.transform()
    # tweets.pprint()
    # tweets.count().pprint()
    #
    # streaming_context.start()
    # streaming_context.awaitTermination()


if __name__ == "__main__":
    # Configure Spark
    spark_session = SparkSession.builder \
        .master("local[*]") \
        .appName(APP_NAME) \
        .config('spark.jars.packages', 'org.apache.spark:spark-streaming-kafka-0-8_2.11:2.2.0,org.mongodb.spark:mongo-spark-connector_2.11:2.2.0') \
        .config("spark.mongodb.input.uri", "mongodb://127.0.0.1/TweetsAnalysis.TestTweets") \
        .config("spark.mongodb.output.uri", "mongodb://127.0.0.1/test.coll") \
        .getOrCreate()

    spark_session.sparkContext.setLogLevel('ERROR')
    kafka_config = json.load(open('kafka.json'))

    main(spark_session, kafka_config)
