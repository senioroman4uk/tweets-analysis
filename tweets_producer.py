import json

from tweepy import OAuthHandler, Stream
from pykafka import KafkaClient

from TweetsBuffer import TweetsBuffer
from TweetsListener import TweetsListener
from TweetsProducerThread import TweetsProducerThread

twitter_credentials = json.load(open('twitter.json'))
auth = OAuthHandler(twitter_credentials['consumerKey'], twitter_credentials['consumerSecret'])
auth.set_access_token(twitter_credentials['accessToken'], twitter_credentials['accessTokenSecret'])


buffer = TweetsBuffer()
twitter_stream = Stream(auth, TweetsListener(buffer))
twitter_stream.filter(track=['trump'], async=True)


kafka_config = json.load(open('kafka.json'))
client = KafkaClient(hosts=kafka_config['hosts'], zookeeper_hosts=kafka_config['zookeeperHosts'])

topic_name = kafka_config['topic']
topic = client.topics[topic_name.encode()]

with topic.get_producer(delivery_reports=kafka_config['processDelivery']) as producer:
    tweets_producer = TweetsProducerThread(buffer, producer, kafka_config['processDelivery'])
    tweets_producer.start()
    tweets_producer.join()
